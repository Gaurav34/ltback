const mongoose = require("mongoose");
// const SurveyTypes = require("../graphql/types/surveys");
const gamedata = new mongoose.Schema(
  {
    gameName: { type: String, required: true },
    // phoneNumber: { type: String, required: true },
    userScore: { type: Number, required: true },
  
    email: { type: String, required: true },
    name: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const gameModel = mongoose.model("gamedata", gamedata);
module.exports = gameModel;


// gameName: req.body.gameName,
//       phoneNumber:  req.body.phoneNumber,
//       userScore: parseInt(req.body.userScore),
//       name:req.body.name,
//       email:req.body.email